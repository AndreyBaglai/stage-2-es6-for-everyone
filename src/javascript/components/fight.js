import { controls } from '../../constants/controls';
import { fighterDetailsMap } from './fighterSelector';
import { showWinnerModal } from './modal/winner';

export async function fight(fighters) {
  const fightersArr = [];
  const scaleHP = 3;
  let isFinished = false;
  
  for (let value of fighters.values()) {
    fightersArr.push(value);
  }

  const [ firstFighter, secondFighter ] = fightersArr;
  const leftIndicator = document.getElementById('left-fighter-indicator');
  const rightIndicator = document.getElementById('right-fighter-indicator');
  
  leftIndicator.style.width = `${firstFighter.health * scaleHP}px`;
  rightIndicator.style.width = `${secondFighter.health * scaleHP}px`;;

  document.addEventListener('keyup', e => {

    if (e.code === 'KeyA') {
      let damage = getDamage(firstFighter, secondFighter);
      secondFighter.health -= damage;
      rightIndicator.style.width = `${secondFighter.health * scaleHP}px`;
      isFinished = isFinish(secondFighter.health);
      if (isFinished) {
        return new Promise((resolve) => {
          // resolve the promise with the winner when fight is over
          const { name } = firstFighter;
          if (isFinished) {
            showWinnerModal({ title: name, bodyElement: 'Is winner!' });
          }
        });
      }
    }

    if (e.code === 'KeyJ') {
      let damage = getDamage(secondFighter, firstFighter);
      firstFighter.health -= damage;
      leftIndicator.style.width = `${firstFighter.health * scaleHP}px`;
      isFinished = isFinish(firstFighter.health);
      if (isFinished) {
        // resolve the promise with the winner when fight is over
        return new Promise((resolve) => {
          const { name } = secondFighter;
          if (isFinished) {
            showWinnerModal({ title: name, bodyElement: 'Is winner!' });
          }
        });
      }
    }
  });
}

function isFinish(health) {
  return health <= 0 ? true : false;
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance  = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
   // return block power
  const dodgeChance  = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
