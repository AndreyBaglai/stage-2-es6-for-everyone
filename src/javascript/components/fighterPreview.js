import { createElement } from '../helpers/domHelper';
import { createImage } from './fightersView';
import { fighterService } from '../services/fightersService';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter === undefined) {
    return '';
  }

  // todo: show fighter info (image, name, health, etc.)
  fighterElement.append(showFighterInfo(fighter));

  return fighterElement;
}

function showFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter preview' });
  const fighterStatistic = createElement({ tagName: 'div', className: 'fighters___statistic' });

  const image = createFighterImage(fighter);
  fighterElement.append(image);

  fighterStatistic.innerHTML = `<ul class="info">
      <li>${name}</li>
      <li>HP: ${health}</li>
      <li>AT: ${attack}</li>
      <li>DF: ${defense}</li>
    </ul>`;
  
  fighterElement.append(fighterStatistic);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
